package hello;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.invoke.MethodHandles;
import java.lang.management.ManagementFactory;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicLong;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.http.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class GreetingController {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	private static final String KAIROSDB_IP_ADDRESS = "52.8.104.253";
	private static final Integer PORT = 4343;

	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();
	// public static Map<BadKey,String> leakMap = new HashMap<>();
	public static Map<BadKey, String> BAD_KEY_MAP = new HashMap<BadKey, String>();
	public static String MEMORY_LEAK_TEST_STRING = "";

	private static final String PREFIX = "put ";

	public static long TIMESTAMP = 0;
	public static int POSTGRES_NUM_OPS_METRIC_COUNT = 0;

	public static final String JDBC_DRIVER = "org.postgresql.Driver";  
	public static final String DB_URL = "jdbc:postgresql://54.193.82.193:5432/opsmx";
	//	public static final String DB_URL = "jdbc:postgresql://localhost:5432/opsmx";


	public static final String USER = "postgres";
	public static final String PASS = "networks123";
	public static String testData = "latency";

	@RequestMapping("/greeting")
	public String greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        LOG.info("Entering Greeting..");
		//LOG.info("Starting point in the rest controller api to get log analysis");
		//LOG.info("Enable lines from 67 to 75 for introducing memory leak for system behaviour");
		//LOG.info("Enable lines from 84 to 90 for introducing Architectural Regression for system behaviour");
		
		// ***** memory leakdemonstrating  *****//
		/*
		if (MEMORY_LEAK_TEST_STRING == null || MEMORY_LEAK_TEST_STRING.length() == 0) {
			for (int i = 0; i < 30000; i++) {
				String test = "" + i + "" + i + "" + i;
				MEMORY_LEAK_TEST_STRING += test;
		         }
		 } else {
		      String suffix = "0a1b2c3d4e5";
		      MEMORY_LEAK_TEST_STRING += suffix;
		}
		BadKey badKey = new BadKey("");
		badKey = new BadKey("" + (new Date().getTime()));
		  //Commented the following line, causes problems with mem util 
	        BAD_KEY_MAP.put(badKey, "" + (new Date().getTime()) + "-" + (new Date().getTime()) + MEMORY_LEAK_TEST_STRING);		  
		 */
		//***End of Memory leak code ***//

		//Demonstrating Architectural Regression/*	POSTGRES_NUM_OPS_METRIC_COUNT += 1;
		//Architectural Regression implementation has been shifted to ArchRegress.java, here only calling thread class.

		//ArchRegress test = new ArchRegress();
		//	Thread th = new Thread(test);
		//	th.start();

		// **** End of architectural regression code ***//

		/*if (Long.compare(TIMESTAMP, 0l) == 0
				|| Long.compare(((new Date()).getTime() - TIMESTAMP), 5000) >= 0) {
			TIMESTAMP = (new Date()).getTime();

			try (Socket clientSocket = new Socket(KAIROSDB_IP_ADDRESS, PORT);
					PrintWriter out = (clientSocket.isConnected()) ? new PrintWriter(
							clientSocket.getOutputStream(), true) : null;) {
				if (out != null) {
					LOG.info("Connected with the server : {} with port : {}",
							clientSocket.getInetAddress().getHostName(),
							clientSocket.getPort());
					writeIntoKairosDB(out, "tomcat.dbOperations", ""+ POSTGRES_NUM_OPS_METRIC_COUNT);
					// writeIntoKairosDB(out, "elasticsearch.num_of_calls",
					// "1");
				}
			} catch (IOException ex) {
				LOG.error("Error: ", ex);
			}
		}*/

		//System.out.println("Adding small difference");
		// System.out.println("HashMap size : "+ leakMap.size());
		// new Greeting(MetricsRegistryClient.client().incrRequestCount(),String.format(template, name))
		//return "HashMap size  : "+ BAD_KEY_MAP.size() + "\n String length  : " + MEMORY_LEAK_TEST_STRING.length() +"\n ";
		String query;
		StringBuffer sb = new StringBuffer();
		String query2;
		StringBuffer sb2 = new StringBuffer();

		ClassLoader cl = GreetingController.class.getClassLoader();
		BufferedReader br;
		try {
			br = new BufferedReader(new InputStreamReader(cl.getResource("testpage.html").openStream()));

			while((query=br.readLine())!=null)
				sb.append(query);	
			br.close();

			String whichdog = "img10";
			/* Introducing Arch. regression*/
			//			      if(10.0*Math.random()>5.0)
			//			       whichdog = "dog2"; // heavier file (1MB)
			/*till here*/

			BufferedReader br2 = new BufferedReader(new InputStreamReader(cl.getResource(whichdog).openStream()));
			while((query2=br2.readLine())!=null)
				sb2.append(query2);	
			br2.close();
		} catch (IOException e) {
			LOG.error("IOException : Unable to open stream for the resource page at : testpage.html ");
		}



		return sb.length()>0 ? sb.toString().replace("##dogimage##", sb2.toString()) : " No page found";
		// 	return "POSTGRES_NUM_OPS_METRIC_COUNT : "+ POSTGRES_NUM_OPS_METRIC_COUNT;
	}


	/*public void getAllUsersFromDB(){
		       Connection c = null;
		       Statement stmt = null;
		       try {
		       Class.forName(JDBC_DRIVER);
		         c = DriverManager.getConnection(DB_URL,USER, PASS);
		         c.setAutoCommit(false);
		         stmt = c.createStatement();
		         ResultSet rs = stmt.executeQuery( "select * from UserLoginDetails" );
		         while(rs.next())  {
		        	 System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3));
		        	 break;
		         }
		         rs.close();
		         stmt.close();
		         c.close();
		       } catch ( Exception e ) {
		         System.err.println( e.getClass().getName()+": "+ e.getMessage() );
		         System.exit(0);
		       }
		       System.out.println("Operation done successfully");

	  }*/

	@RequestMapping("/dogcount")
	public String dogCount() {
		Thread t = new Thread();
		try {
			// Latency 
			t.sleep(0);
		} catch (InterruptedException e) {
			LOG.error("Encountered java.lang.InterruptedException due to increase in latency for api : /dogcount");
		}

		return "{ \"dogCount\": 1.0 }";
	}


	@RequestMapping("/readfile")
	public String readFile(@RequestParam("id") String id, @RequestParam("version") String version, @RequestParam("filename") String fileName) {
		try {
			String file = "/logs/"+ id + "/" + version + "/" + fileName;
			try (InputStream is = this.getClass().getResourceAsStream(file);) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(is));
				while(reader.ready()) {
					System.out.println(reader.readLine());
				}
			} catch (IOException e) {
				return e.getMessage();
			}
			
			
		}catch(Exception ee) {
			return "fail";
		}
		return "success";
	}

	@RequestMapping("/catcount")
	public String catCount() {
		Thread t = new Thread();
		try {
			t.sleep(0);
		} catch (InterruptedException e) {
			LOG.error("Encountered java.lang.InterruptedException due to increase in latency for api : /catcount");
		}
		String result="{ \"catCount\": 1.0 }";
		/*Code for Architectural Regression, prerequisite is to have restapp running on k8 pod */
		/*try {
			for (int i=0; i<=1000; i++)
			{
				URL url = new URL("http://35.192.98.201:8080/catcount");
				HttpURLConnection con = (HttpURLConnection)url.openConnection();
				con.setRequestMethod("GET");
				con.setDoOutput(true);
				con.setInstanceFollowRedirects(false);

				System.out.println("con.getResponseCode() ::"+con.getResponseCode());
				System.out.println(con.getResponseMessage());

				BufferedReader br2 = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String response2; StringBuffer res2 = new StringBuffer();
				while ((response2=br2.readLine())!=null)
					res2.append(response2);

				System.out.println(res2);
				if (!res2.toString().isEmpty())
					result=res2.toString();
			}

		}catch(MalformedURLException mue){
			mue.printStackTrace();
		}catch(IOException io){
			io.printStackTrace();
		}
		/* till here */

		return result;
	}

	@RequestMapping("/status")
	public String getStatus() {
		return " @@@@@@ Welcome to  Strategy World  @@@@@@";
	}


	@RequestMapping("/exceptionGenerator")
	public void exceptionGenerator(String userName) {
		String propertiesFilePath = "/home/ubuntu/"+ userName +"/config.properties";
		try 
		{
			BufferedReader br = new BufferedReader(new FileReader(propertiesFilePath));
		} 
		catch(FileNotFoundException e) {
			LOG.error("Unable to find properties file : java.io.FileNotFoundException at void hello.PetController.getPets() line number "+propertiesFilePath);
		}
	}
	@RequestMapping("/internalError")
	public void internalError() {
		// Null pointer exception for log analasis
		try 
		{
			LOG.info("Inside try block for internalError api");
			Double d= 15.15/0;
			d= d+1;
			throw new RuntimeException();

		} 
		catch(Exception e) {
			LOG.error("Encountered runtime exception : java.lang.RuntimeException at void hello.GreetingController.getPetPrice() line number 253.");
		}
	}
	
	@RequestMapping("/getPetTypes")
	public void getPetTypes() {
		// Null pointer exception for log analysis
		try 
		{
			LOG.info("Inside getPetTypes api");
			throw new ParseException();
		} 
		catch(Exception e) {
			LOG.error("Unable to parse the json data : org.json.ParseException.ParseException() at void hello.GreetingController.getPetTypes() line number 268.");
		}
	}

	@RequestMapping(value = "/mbeans")
	public void mbeans() throws Exception {
		LOG.info("New changes in mbeans");
		MBeanServer server = ManagementFactory.getPlatformMBeanServer();
		//		Set mbeans = server.queryNames(null, null);
		/*
		 * for (Object mbean : mbeans) { WriteAttributes(server,
		 * (ObjectName)mbean); }
		 */

		// RequestProcessor
		ObjectName requestObjName = new ObjectName(	"Tomcat:type=GlobalRequestProcessor,name=*");
		Set<ObjectName> requestObjNameSet = server.queryNames(requestObjName,
				null);
		Long maxProcessingTime = 0L;
		Long processingTime = 0L;
		Long requstCount = 0L;
		Long errorCount = 0L;
		BigDecimal bytesReceived = BigDecimal.ZERO;
		BigDecimal bytesSend = BigDecimal.ZERO;
		for (ObjectName obj : requestObjNameSet) {
			long nowMaxProcessingTime = Long.parseLong(server.getAttribute(obj,
					"maxTime").toString());
			if (maxProcessingTime < nowMaxProcessingTime)
				maxProcessingTime = nowMaxProcessingTime;
			processingTime += Long.parseLong(server.getAttribute(obj,
					"processingTime").toString());
			requstCount += Long.parseLong(server.getAttribute(obj,
					"requestCount").toString());
			errorCount += Long.parseLong(server.getAttribute(obj, "errorCount")
					.toString());
			bytesReceived = bytesReceived.add(new BigDecimal(server
					.getAttribute(obj, "bytesReceived").toString()));
			bytesSend = bytesSend.add(new BigDecimal(server.getAttribute(obj,
					"bytesSent").toString()));
			LOG.info(processingTime + " : " + requstCount + " : "
					+ bytesReceived + " : " + bytesSend);
		}
	}


	private void writeIntoKairosDB(PrintWriter out, String metricName,
			String value) {

		if (metricName != null) {
			LOG.info("The metric name and the key is : {}, {}", metricName,
					value);

			if (value == null) {
				LOG.warn(
						"Skipping write to Kairos as the response has no data for '{}' url",
						metricName);
				return;
			}

			StringBuilder putCommand = new StringBuilder();
			Long timeStamp = System.currentTimeMillis() / 1000;
			putCommand.append(PREFIX);
			putCommand.append(metricName + " ");
			putCommand.append(timeStamp + " ");
			putCommand.append(value + " ");

			String hostname = "Unknown";
			try
			{
				InetAddress addr;
				addr = InetAddress.getLocalHost();
				hostname = addr.getHostName();
			}
			catch (UnknownHostException ex)
			{
				LOG.error("Unknown Hostname could not be resolved : thesherlock.com");
			}

			putCommand.append("host="+hostname+" \n");

			// LOG.info("Writing to KairosDb : {}", putCommand.toString());
			LOG.info("Writing to KairosDb : {}"
					+ putCommand.toString());
			out.write(putCommand.toString());

		}
		out.close();
	}

	//	private void WriteAttributes(final MBeanServer mBeanServer,
	//			final ObjectName http) throws InstanceNotFoundException,
	//			IntrospectionException, ReflectionException {
	//		MBeanInfo info = mBeanServer.getMBeanInfo(http);
	//		MBeanAttributeInfo[] attrInfo = info.getAttributes();
	//
	//		System.out.println("Attributes for object: " + http + ":\n");
	//		for (MBeanAttributeInfo attr : attrInfo) {
	//			System.out.println("  " + attr.getName() + "\n");
	//		}
	//	}

	static class BadKey {
		public final String key;

		public BadKey(String key) {
			this.key = key;
		}
	}

}

